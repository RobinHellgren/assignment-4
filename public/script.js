/*=====================================================================================
*           STATE VARIABLES
=====================================================================================*/
let loanAmount = 0;
let balance = 0;
let purchasedLaptopAfterLoan = true;
let pay = 0;
let computers = [];
let currentComputer;
/*=====================================================================================
*           HTML ELEMENT VARIABLES
=====================================================================================*/
let loanDiv;
let balanceRow;
let loanAmountRow;
let getLoanButton;
let payRow;
let repayButton;
let laptopSelect;
let laptopFeatures;
let laptopTitle;
let laptopDescription;
let laptopPrice;
let laptopImg;
let buyNowButton;

/*=====================================================================================
*           WINDOW INIT FUNCTION
=====================================================================================*/
window.onload = init;

function init(){
    loanDiv = document.getElementById("loan-div");
    balanceRow = document.getElementById("customer-balance");
    loanAmountRow = document.getElementById("customer-loan-balance");
    getLoanButton = document.getElementById("get-loan-button");
    payRow = document.getElementById("customer-pay");
    repayButton = document.getElementById("repay-button");
    laptopSelect = document.getElementById("laptop-menu");
    laptopFeatures = document.getElementById("laptop-features");
    laptopTitle = document.getElementById("laptop-view-name");
    laptopDescription = document.getElementById("laptop-view-description");
    laptopPrice = document.getElementById("laptop-view-price");
    laptopImg = document.getElementById("laptop-img")
    buyNowButton = document.getElementById("buy-now-button")
    fetchAPIData();
    updateDOMLoanRow();
    updateDOMBalanceRow();
    updateDOMPayRow();
    onLaptopSelect();
}

/*=====================================================================================
*           STATE CHANGING METHODS
=====================================================================================*/

//Fetches data from the api and updates the computers state variable with data from the response
async function fetchAPIData(){
    let json;
    try{
        let response = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        json = await response.json();
    }
    catch(error){
        console.error(error);
    }
    json.forEach(computer => {
        computers.push(computer);
    });
    updateDOMLaptopSelect();
}

//Promts the user for an amount to borrow and updates the loanAmount and balance 
// state variable. Then calls DOM updating funtions to display the change.
function getALoan(){
    let amount = window.prompt("Enter the balance of the loan you want to take.")
    amount = parseInt(amount);
    if(amount === "" || amount === null){
        alert("Please enter a valid amount")
        return;
    }
    else if(amount <= balance*2){
        loanAmount += amount;
        balance += amount;
        updateDOMLoanRow();
        updateDOMBalanceRow();
        updateDOMGetLoanButton();
        updateDOMRepayButton();
        purchasedLaptopAfterLoan = false;
    }
    else if(amount > balance*2){
        alert("You can't take a loan greater than twice your current balance");
    }
}
//Substracts the value stored in the pay state variable form the loanAmount state variable if the pay is lower than the loanAmount
//loanAmount is subtracted from pay if pay is greater than the loanAmount. Then updates the DOM the reflect the change.
function repayLoan(){
    if(pay >= loanAmount){
        pay -= loanAmount;
        loanAmount = 0;
    }
    else {
        loanAmount -= pay;
        pay = 0;
    }
    updateDOMLoanRow();
    updateDOMPayRow();
    updateDOMRepayButton();
    updateDOMGetLoanButton();
}

//Adds 100 to the pay state variable and then updates the DOM to reflect the change
function work(){
    pay += 100;
    updateDOMPayRow();
}

//Moves the value stored in the pay state variable to the users balance. If the loanAmount is greater than 0, 10% of pay is used to 
//reduce the loan amount. If the bankCutOfPay is greater than the loan amount, the surplus is added back into the pay.
function bankPay(){
    if(loanAmount > 0){
        let bankCutOfPay = (pay/100)*10;
        pay -= bankCutOfPay;
        if(bankCutOfPay > loanAmount){
            bankCutOfPay -= loanAmount;
            loanAmount = 0;
            pay += bankCutOfPay;
        }
        else{
            loanAmount -= bankCutOfPay;
        }
    }
    balance += pay;
    pay = 0;
    updateDOMBalanceRow();
    updateDOMLoanRow();
    updateDOMPayRow();
}

//Buys a laptop by checking if the balance state variable is greater than the price of the currently selected laptop, if it is reduce 
//the balance by the laptop price and display a message, Otherwise just display a message telling the user that their balance isn't 
//high enough
function buyLaptop(){
    if(balance >= currentComputer.price){
        balance -= currentComputer.price;
        updateDOMBalanceRow();
        alert("Congratulations on your new laptop!");
    }
    else {
        alert("You can't afford that laptop ://")
    }
}
//Sets the currentComputer state variable when a laptop is selected in the dropdown menu and calls DOM update methods
function onLaptopSelect(){
    currentComputer = computers.find(computer => computer.id == laptopSelect.value);
    updateDOMLaptopView();
    updateDOMLaptopFeatures();
}

/*=====================================================================================
*           DOM UPDATE METHODS
=====================================================================================*/

// Shows/hides the get loan button if there's no active loan and the user has purchased a laptop after taking a loan  
function updateDOMGetLoanButton(){
    if(loanAmount > 0){
        getLoanButton.style.display = 'none';
    }
    else if(purchasedLaptopAfterLoan){
        getLoanButton.style.display = 'initial';
    }
}

// Shows/hides the loanAmount row if the user has an active loan, if there is an active loan update text to display current amount
function updateDOMLoanRow() {
    if(loanAmount > 0){
        loanDiv.style.display = "flex"
        loanAmountRow.innerHTML = loanAmount + " SEK";
    }
    else {
        loanDiv.style.display = "none";
    }
}

// Updates the balance amount row to reflect the current balance
function updateDOMBalanceRow(){
    balanceRow.innerHTML = balance + " SEK"
}

// Updates the pay amount row to reflect the current balance
function updateDOMPayRow(){
    payRow.innerHTML = pay + " SEK"
}

// Shows/hides the repay loan button depending on if the user has an active loan or not
function updateDOMRepayButton(){
    if(loanAmount > 0){
        repayButton.style.display = "initial"
    }
    else {
        repayButton.style.display = "none";
    }
}

// Updates the laptop select element to show an option for all laptops contained in the computers state variable
function updateDOMLaptopSelect(){
    computers.forEach(computer => {
        let opt = document.createElement('option');
        opt.value = computer.id;
        opt.innerHTML = computer.title;
        laptopSelect.appendChild(opt);
    });
}

// Updates the DOM to show the attribute stored in the specs array of the currently selected laptop
function updateDOMLaptopFeatures(){
    if(currentComputer !== undefined && currentComputer.id > 0){
        laptopFeatures.innerHTML = '';
        currentComputer.specs.forEach(feature => {
            var li = document.createElement("li");
            li.innerHTML = feature;
            laptopFeatures.appendChild(li);
        })
    }
}

//Updates the DOM to display the properties of the currently selected computer
function updateDOMLaptopView(){
    if(currentComputer !== undefined && currentComputer.id > 0){  
        laptopTitle.innerHTML = currentComputer.title;
        laptopDescription.innerHTML = currentComputer.description;
        laptopPrice.innerHTML = currentComputer.price + " SEK";
        laptopImg.style.display = "initial"
        laptopImg.src = 'https://noroff-komputer-store-api.herokuapp.com/' + currentComputer.image;
        buyNowButton.style.display = "initial";
    }
    else{
        laptopTitle.innerHTML = '';
        laptopDescription.innerHTML = '';
        laptopPrice.innerHTML = '';
        laptopImg.style.display = "none";
        buyNowButton.style.display = "none";
    }
}